# GitLab Agent (agentk) Permissioned Helm Chart

This repository contains a Helm Chart to deploy the GitLab Agent (agentk) permissioned by default in contrast to the [Helm Chart provided by the GitLab.org Team](https://gitlab.com/gitlab-org/charts/gitlab-agent), which grants `cluster-admin` privileges by default.

**Please note:** This project is a community helm chart from GitLab users to GitLab users.
This chart is **not** officially supported by the GitLab company.


## Project Purpose

The [official Helm Chart](https://gitlab.com/gitlab-org/charts/gitlab-agent) for the GitLab Kubernetes Agent (agentk) by default grants `cluster-admin` privileges to the agent.
While this can be disabled, there is no simple feature (yet?) in the official chart to limit access to certain namespaces.
For this, manual manifests have to be written.

This chart solves the problem by allowing to define namespaces to which the agent should have access to, using `Roles` and `RoleBindings`.

## Installation

### Add Helm Repository

```
helm repo add mlohr https://helm-charts.mlohr.com/
helm repo update
```

### Install to Kubernetes

Quick installation:

```
helm upgrade --install gitlab-agent-permissioned mlohr/gitlab-agent-permissioned \
    --set config.token="<YOUR GITLAB.COM AGENT TOKEN>"
```

Define RBAC rules for two namespaces:

```yaml
# custom values.yaml
rbac:
  namespaces:
    # Only allow read access to Pods in the `namespace1` namespace
    - name: namespace1
      rules:
        - apiGroups: [""]
          resources: ["pods"]
          verbs: ["get", "watch", "list"]

    # Allow read/write access in namespace `namespace2`
    - name: namespace2
      rules:
        - apiGroups: ["", "extentions", "apps", "batch", "networking.k8s.io"]
          resources: ["*"]
          verbs: ["*"]
```


For a documentation of all configuration possibilities check [values.yaml](https://gitlab.com/MatthiasLohr/gitlab-agent-permissioned-helm-chart/-/blob/main/values.yaml).


## License

This project is published under the Apache License, Version 2.0.
See [LICENSE.md](https://gitlab.com/MatthiasLohr/gitlab-agent-permissioned-helm-chart/-/blob/main/LICENSE.md) for more information.

Copyright (c) by [Matthias Lohr](https://mlohr.com/) &lt;[mail@mlohr.com](mailto:mail@mlohr.com)&gt;
