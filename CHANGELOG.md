# Changelog

## v1.23.0

  * Upgraded GitLab Agent to v17.8.1

## v1.22.0

  * Upgraded GitLab Agent to v17.8.0

## v1.21.0

  * Upgraded GitLab Agent to v17.7.2

## v1.20.0

  * Upgraded GitLab Agent to v17.6.3

## v1.19.0

  * Upgraded GitLab Agent to v17.6.1

## v1.18.0

  * Upgraded GitLab Agent to v17.6.0

## v1.17.0

  * Upgraded GitLab Agent to v17.5.3

## v1.16.0

  * Upgraded GitLab Agent to v17.5.2

## v1.15.0

  * Upgraded GitLab Agent to v17.5.1

## v1.14.0

  * Upgraded GitLab Agent to v17.4.2

## v1.13.0

  * Upgraded GitLab Agent to v17.4.1

## v1.12.0

  * Upgraded GitLab Agent to v17.4.0

## v1.11.0

  * Upgraded GitLab Agent to v17.3.3

## v1.10.0

  * Upgraded GitLab Agent to v17.2.7

## v1.9.0

  * Upgraded GitLab Agent to v17.2.2

## v1.8.0

  * Upgraded GitLab Agent to v17.2.1

## v1.7.0

  * Upgraded GitLab Agent to v17.2.0

## v1.6.0

  * Upgraded GitLab Agent to v17.1.2

## v1.5.0

  * Upgraded GitLab Agent to v17.1.1

## v1.4.0

  * Upgraded GitLab Agent to v17.1.0

## v1.3.0

  * Upgraded GitLab Agent to v17.0.2

## v1.2.0

  * Upgraded GitLab Agent to v17.0.1

## v1.1.0

  * Upgraded GitLab Agent to v16.11.0

## v1.0.13

  * Upgraded GitLab Agent to v16.10.1

## v1.0.12

  * Upgraded GitLab Agent to v16.8.0

## v1.0.11

  * Added additional Role/ClusterRole rules internally required by GitLab Agent.

## v1.0.10

  * Upgraded GitLab Agent to v16.7.0
  * Upgraded `bitnami/common` to v2.14.0

## v1.0.9

  * Upgraded GitLab Agent to v16.5.0

## v1.0.8

  * Upgraded GitLab Agent to v16.4.0
  * Upgraded `bitnami/common` to v2.12.0

## v1.0.7

  * Upgraded GitLab Agent to v16.2.0
  * Upgraded `bitnami/common` to v2.6.0

## v1.0.6

  * Upgraded GitLab Agent to v16.1.3
  * Upgraded `bitnami/common` to v2.4.0

## v1.0.5

  * Fix: RoleBinding: explicitely define namespace of ServiceAccount 

## v1.0.4

  * Upgraded GitLab Agent to v15.11.0
  * Upgraded `bitnami/common` to v2.2.4

## v1.0.3

  * Fixed whitespace trimming

## v1.0.2

  * Fixed icon URL

## v1.0.1

  * Improved project documentation

## v1.0.0

  * First release
